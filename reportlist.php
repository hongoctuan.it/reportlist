
<?php include 'template/header.php'?>
<body class="g-sidenav-show  bg-gray-200">
  <?php include 'template/left_menu.php'?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">Report List</h6>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">QC team</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        $dir    = '/Users/tuan.hongoc/spqa-automation/project/'.$_GET['project'].'/test_report';
                        // $dir = 'test_report/'.$_GET['project'];
                        $files1 = scandir($dir);
                        // unset($files1[0]);
                        // unset($files1[1]);
                        // unset($files1[2]);
                        // unset($files1[3]);
                        // unset($files1[4]);
                    ?>
                    <?php foreach($files1 as $item):?>
                      <?php if(strpos($item,"html") > 0):?>
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1">
                                <div>
                                    <img src="assets/img/team-2.jpg" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm"><?php echo $item ?></h6>
                                    <!-- <p class="text-xs text-secondary mb-0">john@creative-tim.com</p> -->
                                </div>
                                </div>
                            </td>
                           
                            <td class="align-middle text-center text-sm">
                              <div class="mx-3">
                                <button onclick="viewreport('<?php echo $_GET['project'] ?>','<?php echo $item ?>')" class="btn bg-gradient-primary mt-2 w-80" type="button" id="run_system">View report detail</button>
                                <button onclick="deletereport('<?php echo $_GET['project'] ?>','<?php echo $item ?>')" class="btn bg-gradient-warning mt-2 w-20" type="button" id="run_system">Delete Report</button>

                              </div>
                            </td>
                        </tr>
                      <?php endif?>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
                <script>
                  function viewreport(project, file_name){
                    $.ajax({url: "viewreport.php?project="+project+"&file_name="+file_name, success: function(result){}});                  
                  }
                  function deletereport(project, file_name){
                    $.ajax({url: "deletereport.php?project="+project+"&file_name="+file_name, success: function(result){alert(result); location.reload();}});                  
                  }
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <?php include 'template/js_file.php'?>
</body>

</html>